-- Stephen J. Barr
-- p. 1015 of Book v. 0.9.0.

import Data.Monoid

data Optional a =
  Nada
  | Only a
  deriving (Eq, Show)

instance Monoid a => Monoid (Optional a) where
  mempty                     = Nada
  mappend Nada     Nada      = Nada
  mappend Nada     (Only a)  = Only a
  mappend (Only a) Nada      = Only a
  mappend (Only a) (Only b)  = Only (a <> b)

main :: IO ()
main = do
  print "Only (Sum 1) <> Only (Sum 1):"
  print $ Only (Sum 1) <> Only (Sum 1)
  print "----------------------------------------"
  print "Only (Sum 1) <> Nada"
  print $ Only (Sum 1) <> Nada
